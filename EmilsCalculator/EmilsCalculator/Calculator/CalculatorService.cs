﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmilsCalculator.Calculator
{
    public class CalculatorService
    {
        public long Add(long firstNumber, long secondNumber)
        {
            return firstNumber + secondNumber;
        }

        public long Subtrackt(long firstNumber, long secondNumber)
        {
            return firstNumber - secondNumber;
        }

        public long Multiply(long firstNumber, long secondNumber)
        {
            return firstNumber * secondNumber;
        }

        public double Divide(double firstNumber, double secondNumber)
        {
            double result = firstNumber / secondNumber;
            if (firstNumber == 0)
            {
                throw new ArgumentException("Cannot divide by 0");
            }
            return firstNumber / secondNumber;
        }
    }
}
